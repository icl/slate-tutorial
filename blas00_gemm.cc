// blas00_gemm.cc
// BLAS++
#include <blas.hh>

#include "util.hh"

//------------------------------------------------------------------------------
template <typename scalar_type>
void test_gemm()
{
    print_func( 0 );

    scalar_type alpha = 2.0, beta = 1.0;
    int64_t m=200, n=100, k=50, lda=m, ldb=k, ldc=m;
    scalar_type* A = new scalar_type[ lda * k ];  // m x k
    scalar_type* B = new scalar_type[ ldb * n ];  // k x n
    scalar_type* C = new scalar_type[ ldc * n ];  // m x n
    random_matrix( m, k, A, lda );
    random_matrix( k, n, B, ldb );
    random_matrix( m, n, C, ldc );

    blas::gemm( blas::Layout::ColMajor,
                blas::Op::NoTrans, blas::Op::NoTrans, m, n, k,
                alpha, A, lda, B, ldb, beta, C, ldc );

    delete[] A;
    delete[] B;
    delete[] C;
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    test_gemm< float >();
    test_gemm< double >();
    test_gemm< std::complex<float> >();
    test_gemm< std::complex<double> >();
}
