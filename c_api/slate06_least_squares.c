// slate06_least_squares.cc
// Solve over- and under-determined AX = B
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_gels_overdetermined_r32()
{
    print_func( mpi_rank );

    // TODO: failing if m, n not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 BX = slate_Matrix_create_r32(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( BX );
    slate_Matrix_r32 B = slate_Matrix_create_slice_r32( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_r32 X = slate_Matrix_create_slice_r32( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_r32( A );
    random_matrix_type_r32( B );

    // solve AX = B, solution in X
    slate_least_squares_solve_r32( A, BX, 0, NULL );

    slate_Matrix_destroy_r32( A );
    slate_Matrix_destroy_r32( BX );
    slate_Matrix_destroy_r32( B );
    slate_Matrix_destroy_r32( X );
}

//------------------------------------------------------------------------------
void test_gels_underdetermined_r32()
{
    print_func( mpi_rank );

    // TODO: failing if not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 BX = slate_Matrix_create_r32(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( BX );
    slate_Matrix_r32 B = slate_Matrix_create_slice_r32( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_r32 X = slate_Matrix_create_slice_r32( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_r32( A );
    random_matrix_type_r32( B );

    // solve A^H X = B, solution in X
    slate_Matrix_conjTranspose_in_place_r32( A );
    slate_least_squares_solve_r32( A, BX, 0, NULL );

    slate_Matrix_destroy_r32( A );
    slate_Matrix_destroy_r32( BX );
    slate_Matrix_destroy_r32( B );
    slate_Matrix_destroy_r32( X );
}

//------------------------------------------------------------------------------
void test_gels_overdetermined_c32()
{
    print_func( mpi_rank );

    // TODO: failing if m, n not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 BX = slate_Matrix_create_c32(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( BX );
    slate_Matrix_c32 B = slate_Matrix_create_slice_c32( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_c32 X = slate_Matrix_create_slice_c32( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_c32( A );
    random_matrix_type_c32( B );

    // solve AX = B, solution in X
    slate_least_squares_solve_c32( A, BX, 0, NULL );

    slate_Matrix_destroy_c32( A );
    slate_Matrix_destroy_c32( BX );
    slate_Matrix_destroy_c32( B );
    slate_Matrix_destroy_c32( X );
}

//------------------------------------------------------------------------------
void test_gels_overdetermined_r64()
{
    print_func( mpi_rank );

    // TODO: failing if m, n not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 BX = slate_Matrix_create_r64(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( BX );
    slate_Matrix_r64 B = slate_Matrix_create_slice_r64( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_r64 X = slate_Matrix_create_slice_r64( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_r64( A );
    random_matrix_type_r64( B );

    // solve AX = B, solution in X
    slate_least_squares_solve_r64( A, BX, 0, NULL );

    slate_Matrix_destroy_r64( A );
    slate_Matrix_destroy_r64( BX );
    slate_Matrix_destroy_r64( B );
    slate_Matrix_destroy_r64( X );
}

//------------------------------------------------------------------------------
void test_gels_underdetermined_r64()
{
    print_func( mpi_rank );

    // TODO: failing if not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 BX = slate_Matrix_create_r64(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( BX );
    slate_Matrix_r64 B = slate_Matrix_create_slice_r64( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_r64 X = slate_Matrix_create_slice_r64( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_r64( A );
    random_matrix_type_r64( B );

    // solve A^H X = B, solution in X
    slate_Matrix_conjTranspose_in_place_r64( A );
    slate_least_squares_solve_r64( A, BX, 0, NULL );

    slate_Matrix_destroy_r64( A );
    slate_Matrix_destroy_r64( BX );
    slate_Matrix_destroy_r64( B );
    slate_Matrix_destroy_r64( X );
}

//------------------------------------------------------------------------------
void test_gels_underdetermined_c32()
{
    print_func( mpi_rank );

    // TODO: failing if not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 BX = slate_Matrix_create_c32(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( BX );
    slate_Matrix_c32 B = slate_Matrix_create_slice_c32( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_c32 X = slate_Matrix_create_slice_c32( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_c32( A );
    random_matrix_type_c32( B );

    // solve A^H X = B, solution in X
    slate_Matrix_conjTranspose_in_place_c32( A );
    slate_least_squares_solve_c32( A, BX, 0, NULL );

    slate_Matrix_destroy_c32( A );
    slate_Matrix_destroy_c32( BX );
    slate_Matrix_destroy_c32( B );
    slate_Matrix_destroy_c32( X );
}

//------------------------------------------------------------------------------
void test_gels_overdetermined_c64()
{
    print_func( mpi_rank );

    // TODO: failing if m, n not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 BX = slate_Matrix_create_c64(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( BX );
    slate_Matrix_c64 B = slate_Matrix_create_slice_c64( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_c64 X = slate_Matrix_create_slice_c64( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_c64( A );
    random_matrix_type_c64( B );

    // solve AX = B, solution in X
    slate_least_squares_solve_c64( A, BX, 0, NULL );

    slate_Matrix_destroy_c64( A );
    slate_Matrix_destroy_c64( BX );
    slate_Matrix_destroy_c64( B );
    slate_Matrix_destroy_c64( X );
}

//------------------------------------------------------------------------------
void test_gels_underdetermined_c64()
{
    print_func( mpi_rank );

    // TODO: failing if not divisible by nb?
    int64_t m=2000, n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t max_mn = MAX( m, n );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 BX = slate_Matrix_create_c64(
        max_mn, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( BX );
    slate_Matrix_c64 B = slate_Matrix_create_slice_c64( BX, 0, m-1, 0, nrhs-1 );
    slate_Matrix_c64 X = slate_Matrix_create_slice_c64( BX, 0, n-1, 0, nrhs-1 );
    random_matrix_type_c64( A );
    random_matrix_type_c64( B );

    // solve A^H X = B, solution in X
    slate_Matrix_conjTranspose_in_place_c64( A );
    slate_least_squares_solve_c64( A, BX, 0, NULL );

    slate_Matrix_destroy_c64( A );
    slate_Matrix_destroy_c64( BX );
    slate_Matrix_destroy_c64( B );
    slate_Matrix_destroy_c64( X );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    // test_gels_overdetermined< float >();
    // test_gels_overdetermined< double >();
    // test_gels_overdetermined< std::complex<float> >();
    test_gels_overdetermined_c64();

    // test_gels_underdetermined< float >();
    // test_gels_underdetermined< double >();
    // test_gels_underdetermined< std::complex<float> >();
    test_gels_underdetermined_c64();

    err = MPI_Finalize();
    assert( err == 0 );
}
