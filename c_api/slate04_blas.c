// example_04.c
// BLAS routines
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_gemm_r32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        m, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 B = slate_Matrix_create_r32(
        k, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 C = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( B );
    slate_Matrix_insertLocalTiles_r32( C );
    random_matrix_type_r32( A );
    random_matrix_type_r32( B );
    random_matrix_type_r32( C );

    // C = alpha A B + beta C
    slate_multiply_r32( alpha, A, B, beta, C, 0, NULL );

    //--------------------
    // with options
    // slate_Options opts[] = {
    //    { slate_Option_Lookahead, 1 },
    //    { slate_Option_Target,    slate_Target_Devices }  // on GPU devices
    // };
    // slate_multiply_r32( alpha, A, B, beta, C, 0, opts );

    slate_Matrix_destroy_r32( A );
    slate_Matrix_destroy_r32( B );
    slate_Matrix_destroy_r32( C );
}

//------------------------------------------------------------------------------
void test_gemm_r64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        k, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 C = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    slate_Matrix_insertLocalTiles_r64( C );
    random_matrix_type_r64( A );
    random_matrix_type_r64( B );
    random_matrix_type_r64( C );

    // C = alpha A B + beta C
    slate_multiply_r64( alpha, A, B, beta, C, 0, NULL );

    //--------------------
    // with options
    // (1)
    // slate_Options opts[] = {
    //    {slate_Option_ChunkSize,       3},
    //    {slate_Option_Lookahead,       4},
    //    {slate_Option_BlockSize,       256},
    //    {slate_Option_InnerBlocking,   56},
    //    {slate_Option_MaxPanelThreads, 20},
    //    // Error: narrowing conversion from
    //    //        'double' to 'long int'
    //    //        inside { }
    //    // {slate_Option_Tolerance,    1e-12},
    //    {slate_Option_Target,          slate_Target_Devices}
    // };
    //
    // (2)
    // slate_Options opts[ 8 ];
    // opts[0].option = slate_Option_ChunkSize;        opts[0].OptionValue.chunk_size        = 3;
    // opts[1].option = slate_Option_Lookahead;        opts[1].OptionValue.lookahead         = 4;
    // opts[2].option = slate_Option_BlockSize;        opts[2].OptionValue.block_size        = 256;
    // opts[3].option = slate_Option_InnerBlocking;    opts[3].OptionValue.inner_blocking    = 56;
    // opts[4].option = slate_Option_MaxPanelThreads;  opts[4].OptionValue.max_panel_threads = 20;
    // opts[5].option = slate_Option_Tolerance;        opts[5].OptionValue.tolerance         = 1e-12;
    // opts[6].option = slate_Option_Target;           opts[6].OptionValue.target            = slate_Target_Devices;

    slate_Matrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
    slate_Matrix_destroy_r64( C );
}

//------------------------------------------------------------------------------
void test_gemm_c32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        m, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 B = slate_Matrix_create_c32(
        k, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 C = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( B );
    slate_Matrix_insertLocalTiles_c32( C );
    random_matrix_type_c32( A );
    random_matrix_type_c32( B );
    random_matrix_type_c32( C );

    // C = alpha A B + beta C
    slate_multiply_c32( alpha, A, B, beta, C, 0, NULL );

    //--------------------
    // with options
    // slate_Options opts[] = {
    //    { slate_Option_Lookahead, 1 },
    //    { slate_Option_Target,    slate_Target_Devices }  // on GPU devices
    // };
    // slate_multiply_c32( alpha, A, B, beta, C, 0, opts );

    slate_Matrix_destroy_c32( A );
    slate_Matrix_destroy_c32( B );
    slate_Matrix_destroy_c32( C );
}

//------------------------------------------------------------------------------
void test_gemm_c64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        m, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 B = slate_Matrix_create_c64(
        k, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 C = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( B );
    slate_Matrix_insertLocalTiles_c64( C );
    random_matrix_type_c64( A );
    random_matrix_type_c64( B );
    random_matrix_type_c64( C );

    // C = alpha A B + beta C
    slate_multiply_c64( alpha, A, B, beta, C, 0, NULL );

    //--------------------
    // with options
    // slate_Options opts[] = {
    //    { slate_Option_Lookahead, 1 },
    //    { slate_Option_Target,    slate_Target_Devices }  // on GPU devices
    // };
    // slate_multiply_c64( alpha, A, B, beta, C, 0, opts );

    slate_Matrix_destroy_c64( A );
    slate_Matrix_destroy_c64( B );
    slate_Matrix_destroy_c64( C );
}

void test_gemm_transpose_r32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    // Dimensions of X, Y, Z are backwards from A, B, C in test_gemm().
    slate_Matrix_r32 X = slate_Matrix_create_r32(
        k, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 Y = slate_Matrix_create_r32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 Z = slate_Matrix_create_r32(
        n, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( X );
    slate_Matrix_insertLocalTiles_r32( Y );
    slate_Matrix_insertLocalTiles_r32( Z );
    random_matrix_type_r32( X );
    random_matrix_type_r32( Y );
    random_matrix_type_r32( Z );
    slate_Matrix_transpose_in_place_r32( X );
    slate_Matrix_conjTranspose_in_place_r32( Y );
    slate_Matrix_conjTranspose_in_place_r32( Z );

    slate_multiply_r32( alpha, X, Y, beta, Z, 0, NULL );

    slate_Matrix_destroy_r32( X );
    slate_Matrix_destroy_r32( Y );
    slate_Matrix_destroy_r32( Z );
}

void test_gemm_transpose_r64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    // Dimensions of X, Y, Z are backwards from A, B, C in test_gemm().
    slate_Matrix_r64 X = slate_Matrix_create_r64(
        k, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 Y = slate_Matrix_create_r64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 Z = slate_Matrix_create_r64(
        n, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( X );
    slate_Matrix_insertLocalTiles_r64( Y );
    slate_Matrix_insertLocalTiles_r64( Z );
    random_matrix_type_r64( X );
    random_matrix_type_r64( Y );
    random_matrix_type_r64( Z );
    slate_Matrix_transpose_in_place_r64( X );
    slate_Matrix_conjTranspose_in_place_r64( Y );
    slate_Matrix_conjTranspose_in_place_r64( Z );

    slate_multiply_r64( alpha, X, Y, beta, Z, 0, NULL );

    slate_Matrix_destroy_r64( X );
    slate_Matrix_destroy_r64( Y );
    slate_Matrix_destroy_r64( Z );
}

// todo: failing
void test_gemm_transpose_c32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    // Dimensions of X, Y, Z are backwards from A, B, C in test_gemm().
    slate_Matrix_c32 X = slate_Matrix_create_c32(
        k, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 Y = slate_Matrix_create_c32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 Z = slate_Matrix_create_c32(
        n, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( X );
    slate_Matrix_insertLocalTiles_c32( Y );
    slate_Matrix_insertLocalTiles_c32( Z );
    random_matrix_type_c32( X );
    random_matrix_type_c32( Y );
    random_matrix_type_c32( Z );
    slate_Matrix_transpose_in_place_c32( X );
    slate_Matrix_conjTranspose_in_place_c32( Y );
    slate_Matrix_conjTranspose_in_place_c32( Z );

    slate_multiply_c32( alpha, X, Y, beta, Z, 0, NULL );

    slate_Matrix_destroy_c32( X );
    slate_Matrix_destroy_c32( Y );
    slate_Matrix_destroy_c32( Z );
}

// todo: failing
void test_gemm_transpose_c64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    // Dimensions of X, Y, Z are backwards from A, B, C in test_gemm().
    slate_Matrix_c64 X = slate_Matrix_create_c64(
        k, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 Y = slate_Matrix_create_c64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 Z = slate_Matrix_create_c64(
        n, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( X );
    slate_Matrix_insertLocalTiles_c64( Y );
    slate_Matrix_insertLocalTiles_c64( Z );
    random_matrix_type_c64( X );
    random_matrix_type_c64( Y );
    random_matrix_type_c64( Z );
    slate_Matrix_transpose_in_place_c64( X );
    slate_Matrix_conjTranspose_in_place_c64( Y );
    slate_Matrix_conjTranspose_in_place_c64( Z );

    slate_multiply_c64( alpha, X, Y, beta, Z, 0, NULL );

    slate_Matrix_destroy_c64( X );
    slate_Matrix_destroy_c64( Y );
    slate_Matrix_destroy_c64( Z );
}

//------------------------------------------------------------------------------
void test_symm_r32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_r32 A = slate_SymmetricMatrix_create_r32(
        slate_Uplo_Lower, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 B = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 C = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( B );
    slate_Matrix_insertLocalTiles_r32( C );
    random_symmetric_matrix_type_r32( A, slate_Uplo_Lower );
    random_matrix_type_r32( B );
    random_matrix_type_r32( C );

    // C = alpha A B + beta C
    slate_symmetric_left_multiply_r32( alpha, A, B, beta, C, 0, NULL );

    slate_SymmetricMatrix_destroy_r32( A );
    slate_Matrix_destroy_r32( B );
    slate_Matrix_destroy_r32( C );
}

//------------------------------------------------------------------------------
void test_symm_r64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_r64 A = slate_SymmetricMatrix_create_r64(
        slate_Uplo_Lower, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 C = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    slate_Matrix_insertLocalTiles_r64( C );
    random_symmetric_matrix_type_r64( A, slate_Uplo_Lower );
    random_matrix_type_r64( B );
    random_matrix_type_r64( C );

    // C = alpha A B + beta C
    slate_symmetric_left_multiply_r64( alpha, A, B, beta, C, 0, NULL );

    slate_SymmetricMatrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
    slate_Matrix_destroy_r64( C );
}

//------------------------------------------------------------------------------
void test_symm_c32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_c32 A = slate_SymmetricMatrix_create_c32(
        slate_Uplo_Lower, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 B = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 C = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( B );
    slate_Matrix_insertLocalTiles_c32( C );
    random_symmetric_matrix_type_c32( A, slate_Uplo_Lower );
    random_matrix_type_c32( B );
    random_matrix_type_c32( C );

    // C = alpha A B + beta C
    slate_symmetric_left_multiply_c32( alpha, A, B, beta, C, 0, NULL );

    slate_SymmetricMatrix_destroy_c32( A );
    slate_Matrix_destroy_c32( B );
    slate_Matrix_destroy_c32( C );
}

//------------------------------------------------------------------------------
void test_symm_c64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_c64 A = slate_SymmetricMatrix_create_c64(
        slate_Uplo_Lower, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 B = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 C = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( B );
    slate_Matrix_insertLocalTiles_c64( C );
    random_symmetric_matrix_type_c64( A, slate_Uplo_Lower );
    random_matrix_type_c64( B );
    random_matrix_type_c64( C );

    // C = alpha A B + beta C
    slate_symmetric_left_multiply_c64( alpha, A, B, beta, C, 0, NULL );

    slate_SymmetricMatrix_destroy_c64( A );
    slate_Matrix_destroy_c64( B );
    slate_Matrix_destroy_c64( C );
}

//------------------------------------------------------------------------------
void test_syrk_syr2k_r32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 B = slate_Matrix_create_r32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_r32 C = slate_SymmetricMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( B );
    slate_SymmetricMatrix_insertLocalTiles_r32( C );
    random_matrix_type_r32( A );
    random_matrix_type_r32( B );
    random_symmetric_matrix_type_r32( C, slate_Uplo_Lower );

    // C = alpha A A^T + beta C
    slate_symmetric_rank_k_update_r32( alpha, A, beta, C, 0, NULL );

    // C = alpha A B^T + alpha B A^T + beta C
    slate_symmetric_rank_2k_update_r32( alpha, A, B, beta, C, 0, NULL );

    slate_Matrix_destroy_r32( A );
    slate_Matrix_destroy_r32( B );
    slate_SymmetricMatrix_destroy_r32( C );
}

//------------------------------------------------------------------------------
void test_syrk_syr2k_r64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_r64 C = slate_SymmetricMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    slate_SymmetricMatrix_insertLocalTiles_r64( C );
    random_matrix_type_r64( A );
    random_matrix_type_r64( B );
    random_symmetric_matrix_type_r64( C, slate_Uplo_Lower );

    // C = alpha A A^T + beta C
    slate_symmetric_rank_k_update_r64( alpha, A, beta, C, 0, NULL );

    // C = alpha A B^T + alpha B A^T + beta C
    slate_symmetric_rank_2k_update_r64( alpha, A, B, beta, C, 0, NULL );

    slate_Matrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
    slate_SymmetricMatrix_destroy_r64( C );
}

//------------------------------------------------------------------------------
void test_syrk_syr2k_c32()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 B = slate_Matrix_create_c32(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_c32 C = slate_SymmetricMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( B );
    slate_SymmetricMatrix_insertLocalTiles_c32( C );
    random_matrix_type_c32( A );
    random_matrix_type_c32( B );
    random_symmetric_matrix_type_c32( C, slate_Uplo_Lower );

    // C = alpha A A^T + beta C
    slate_symmetric_rank_k_update_c32( alpha, A, beta, C, 0, NULL );

    // C = alpha A B^T + alpha B A^T + beta C
    slate_symmetric_rank_2k_update_c32( alpha, A, B, beta, C, 0, NULL );

    slate_Matrix_destroy_c32( A );
    slate_Matrix_destroy_c32( B );
    slate_SymmetricMatrix_destroy_c32( C );
}

//------------------------------------------------------------------------------
void test_syrk_syr2k_c64()
{
    print_func( mpi_rank );

    double alpha = 2.0, beta = 1.0;
    int64_t n=1000, k=500, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 B = slate_Matrix_create_c64(
        n, k, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_c64 C = slate_SymmetricMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( B );
    slate_SymmetricMatrix_insertLocalTiles_c64( C );
    random_matrix_type_c64( A );
    random_matrix_type_c64( B );
    random_symmetric_matrix_type_c64( C, slate_Uplo_Lower );

    // C = alpha A A^T + beta C
    slate_symmetric_rank_k_update_c64( alpha, A, beta, C, 0, NULL );

    // C = alpha A B^T + alpha B A^T + beta C
    slate_symmetric_rank_2k_update_c64( alpha, A, B, beta, C, 0, NULL );

    slate_Matrix_destroy_c64( A );
    slate_Matrix_destroy_c64( B );
    slate_SymmetricMatrix_destroy_c64( C );
}

//------------------------------------------------------------------------------
void test_trmm_trsm_r32()
{
    print_func( mpi_rank );

    double alpha = 2.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_TriangularMatrix_r32 A = slate_TriangularMatrix_create_r32(
        slate_Uplo_Lower, slate_Diag_NonUnit, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 B = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_TriangularMatrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( B );
    random_triangular_matrix_type_r32( A, slate_Uplo_Lower, slate_Diag_NonUnit );
    random_matrix_type_r32( B );

    // B = alpha A B
    slate_triangular_left_multiply_r32( alpha, A, B, 0, NULL );

    // B = alpha A^{-1} B
    slate_triangular_left_solve_r32( alpha, A, B, 0, NULL );

    slate_TriangularMatrix_destroy_r32( A );
    slate_Matrix_destroy_r32( B );
}

//------------------------------------------------------------------------------
void test_trmm_trsm_r64()
{
    print_func( mpi_rank );

    double alpha = 2.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_TriangularMatrix_r64 A = slate_TriangularMatrix_create_r64(
        slate_Uplo_Lower, slate_Diag_NonUnit, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_TriangularMatrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    random_triangular_matrix_type_r64( A, slate_Uplo_Lower, slate_Diag_NonUnit );
    random_matrix_type_r64( B );

    // B = alpha A B
    slate_triangular_left_multiply_r64( alpha, A, B, 0, NULL );

    // B = alpha A^{-1} B
    slate_triangular_left_solve_r64( alpha, A, B, 0, NULL );

    slate_TriangularMatrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
}

//------------------------------------------------------------------------------
void test_trmm_trsm_c32()
{
    print_func( mpi_rank );

    double alpha = 2.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_TriangularMatrix_c32 A = slate_TriangularMatrix_create_c32(
        slate_Uplo_Lower, slate_Diag_NonUnit, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 B = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_TriangularMatrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( B );
    random_triangular_matrix_type_c32( A, slate_Uplo_Lower, slate_Diag_NonUnit );
    random_matrix_type_c32( B );

    // B = alpha A B
    slate_triangular_left_multiply_c32( alpha, A, B, 0, NULL );

    // B = alpha A^{-1} B
    slate_triangular_left_solve_c32( alpha, A, B, 0, NULL );

    slate_TriangularMatrix_destroy_c32( A );
    slate_Matrix_destroy_c32( B );
}

//------------------------------------------------------------------------------
void test_trmm_trsm_c64()
{
    print_func( mpi_rank );

    double alpha = 2.0;
    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_TriangularMatrix_c64 A = slate_TriangularMatrix_create_c64(
        slate_Uplo_Lower, slate_Diag_NonUnit, m, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 B = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_TriangularMatrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( B );
    random_triangular_matrix_type_c64( A, slate_Uplo_Lower, slate_Diag_NonUnit );
    random_matrix_type_c64( B );

    // B = alpha A B
    slate_triangular_left_multiply_c64( alpha, A, B, 0, NULL );

    // B = alpha A^{-1} B
    slate_triangular_left_solve_c64( alpha, A, B, 0, NULL );

    slate_TriangularMatrix_destroy_c64( A );
    slate_Matrix_destroy_c64( B );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_gemm_r32();
    test_gemm_r64();
    test_gemm_c32();
    test_gemm_c64();

    test_gemm_transpose_r32();
    test_gemm_transpose_r64();
    // test_gemm_transpose_c32(); // todo: failing
    // test_gemm_transpose_c64(); // todo: failing

    test_symm_r32();
    test_symm_r64();
    test_symm_c32();
    test_symm_c64();

    test_syrk_syr2k_r32();
    test_syrk_syr2k_r64();
    test_syrk_syr2k_c32();
    test_syrk_syr2k_c64();

    test_trmm_trsm_r32();
    test_trmm_trsm_r64();
    test_trmm_trsm_c32();
    test_trmm_trsm_c64();

    err = MPI_Finalize();
    assert( err == 0 );
}
