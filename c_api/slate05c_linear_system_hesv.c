// slate05c_linear_system_hesv.c
// Solve AX = B using Aasen's symmetric indefinite factorization
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_hesv_r64()
{
    print_func( mpi_rank );

    int64_t n=1000, nrhs=100, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r64 A = slate_HermitianMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        n, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    random_hermitian_matrix_type_r64( A, slate_Uplo_Lower );
    random_matrix_type_r64( B );

    // workspaces
    // slate_Matrix_r64 H = slate_Matrix_create_r64(
    //    n, n, nb, p, q, MPI_COMM_WORLD );
    // slate_BandMatrix_r64 T = slate_BandMatrix_create_r64(
    //    n, n, nb, nb, nb, p, q, MPI_COMM_WORLD );
    // slate_Pivots pivots  = slate_Pivots_create();
    // slate_Pivots pivots2 = slate_Pivots_create();

    // slate_indefinite_factor_r64( A, pivots, T, pivots2, H );
    slate_indefinite_solve_r64( A, B, 0, NULL );

    slate_HermitianMatrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_hesv_r64();

    err = MPI_Finalize();
    assert( err == 0 );
}
