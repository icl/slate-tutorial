// slate08_hermitian_eig.c
// Solve Hermitian eigenvalues A = V Lambda V^H
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_hermitian_eig_r32()
{
    print_func( mpi_rank );

    // TODO: failing if n not divisible by nb?
    int64_t n=1000, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r32 A = slate_HermitianMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r32( A );
    random_hermitian_matrix_type_r32( A, slate_Uplo_Lower );
    float* Lambda = (float*) malloc(n * sizeof(float));

    slate_hermitian_eig_vals_r32( A, Lambda, 0, NULL );

    slate_HermitianMatrix_destroy_r32( A );
    free( Lambda );
}

//------------------------------------------------------------------------------
void test_hermitian_eig_r64()
{
    print_func( mpi_rank );

    // TODO: failing if n not divisible by nb?
    int64_t n=1000, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r64 A = slate_HermitianMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r64( A );
    random_hermitian_matrix_type_r64( A, slate_Uplo_Lower );
    double* Lambda = (double*) malloc(n * sizeof(double));

    slate_hermitian_eig_vals_r64( A, Lambda, 0, NULL );

    slate_HermitianMatrix_destroy_r64( A );
    free( Lambda );
}

//------------------------------------------------------------------------------
void test_hermitian_eig_c32()
{
    print_func( mpi_rank );

    // TODO: failing if n not divisible by nb?
    int64_t n=1000, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c32 A = slate_HermitianMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c32( A );
    random_hermitian_matrix_type_c32( A, slate_Uplo_Lower );
    float* Lambda = (float*) malloc(n * sizeof(float));

    slate_hermitian_eig_vals_c32( A, Lambda, 0, NULL );

    slate_HermitianMatrix_destroy_c32( A );
    free( Lambda );
}

//------------------------------------------------------------------------------
void test_hermitian_eig_c64()
{
    print_func( mpi_rank );

    // TODO: failing if n not divisible by nb?
    int64_t n=1000, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c64 A = slate_HermitianMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c64( A );
    random_hermitian_matrix_type_c64( A, slate_Uplo_Lower );
    double* Lambda = (double*) malloc(n * sizeof(double));

    slate_hermitian_eig_vals_c64( A, Lambda, 0, NULL );

    slate_HermitianMatrix_destroy_c64( A );
    free( Lambda );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_hermitian_eig_r32();
    test_hermitian_eig_r64();
    test_hermitian_eig_c32();
    test_hermitian_eig_c64();

    err = MPI_Finalize();
    assert( err == 0 );
}
