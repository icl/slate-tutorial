// slate05b_linear_system_cholesky.c
// Solve AX = B using Cholesky factorization
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_cholesky_r32()
{
    print_func( mpi_rank );

    int64_t n=1000, nrhs=100, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r32 A = slate_HermitianMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r32 B = slate_Matrix_create_r32(
        n, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r32( A );
    slate_Matrix_insertLocalTiles_r32( B );
    random_hermitian_matrix_type_diag_dominant_r32( A, slate_Uplo_Lower );
    random_matrix_type_r32( B );

    slate_chol_solve_r32( A, B, 0, NULL );

    slate_HermitianMatrix_destroy_r32( A );
    slate_Matrix_destroy_r32( B );
}

//------------------------------------------------------------------------------
void test_cholesky_r64()
{
    print_func( mpi_rank );

    int64_t n=1000, nrhs=100, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r64 A = slate_HermitianMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_r64 B = slate_Matrix_create_r64(
        n, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r64( A );
    slate_Matrix_insertLocalTiles_r64( B );
    random_hermitian_matrix_type_diag_dominant_r64( A, slate_Uplo_Lower );
    random_matrix_type_r64( B );

    slate_chol_solve_r64( A, B, 0, NULL );

    slate_HermitianMatrix_destroy_r64( A );
    slate_Matrix_destroy_r64( B );
}

//------------------------------------------------------------------------------
void test_cholesky_c32()
{
    print_func( mpi_rank );

    int64_t n=1000, nrhs=100, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c32 A = slate_HermitianMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c32 B = slate_Matrix_create_c32(
        n, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c32( A );
    slate_Matrix_insertLocalTiles_c32( B );
    random_hermitian_matrix_type_diag_dominant_c32( A, slate_Uplo_Lower );
    random_matrix_type_c32( B );

    slate_chol_solve_c32( A, B, 0, NULL );

    slate_HermitianMatrix_destroy_c32( A );
    slate_Matrix_destroy_c32( B );
}

//------------------------------------------------------------------------------
void test_cholesky_c64()
{
    print_func( mpi_rank );

    int64_t n=1000, nrhs=100, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c64 A = slate_HermitianMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_c64 B = slate_Matrix_create_c64(
        n, nrhs, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c64( A );
    slate_Matrix_insertLocalTiles_c64( B );
    random_hermitian_matrix_type_diag_dominant_c64( A, slate_Uplo_Lower );
    random_matrix_type_c64( B );

    slate_chol_solve_c64( A, B, 0, NULL );

    slate_HermitianMatrix_destroy_c64( A );
    slate_Matrix_destroy_c64( B );
}

//------------------------------------------------------------------------------
void test_cholesky_inverse_r32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r32 A = slate_HermitianMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r32( A );
    random_hermitian_matrix_type_diag_dominant_r32( A, slate_Uplo_Lower );

    slate_chol_factor_r32( A, 0, NULL );
    slate_chol_inverse_using_factor_r32( A, 0, NULL );

    slate_HermitianMatrix_destroy_r32( A );
}

//------------------------------------------------------------------------------
void test_cholesky_inverse_r64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r64 A = slate_HermitianMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r64( A );
    random_hermitian_matrix_type_diag_dominant_r64( A, slate_Uplo_Lower );

    slate_chol_factor_r64( A, 0, NULL );
    slate_chol_inverse_using_factor_r64( A, 0, NULL );

    slate_HermitianMatrix_destroy_r64( A );
}

//------------------------------------------------------------------------------
void test_cholesky_inverse_c32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c32 A = slate_HermitianMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c32( A );
    random_hermitian_matrix_type_diag_dominant_c32( A, slate_Uplo_Lower );

    slate_chol_factor_c32( A, 0, NULL );
    slate_chol_inverse_using_factor_c32( A, 0, NULL );

    slate_HermitianMatrix_destroy_c32( A );
}

//------------------------------------------------------------------------------
void test_cholesky_inverse_c64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c64 A = slate_HermitianMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c64( A );
    random_hermitian_matrix_type_diag_dominant_c64( A, slate_Uplo_Lower );

    slate_chol_factor_c64( A, 0, NULL );
    slate_chol_inverse_using_factor_c64( A, 0, NULL );

    slate_HermitianMatrix_destroy_c64( A );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_cholesky_r32();
    test_cholesky_r64();
    test_cholesky_c32();
    test_cholesky_c64();

    test_cholesky_inverse_r32();
    test_cholesky_inverse_r64();
    test_cholesky_inverse_c32();
    test_cholesky_inverse_c64();

    err = MPI_Finalize();
    assert( err == 0 );
}
