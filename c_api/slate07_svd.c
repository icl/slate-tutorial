// slate07_svd.c
// Solve Singular Value Decomposition, A = U Sigma V^H
#include "slate/c_api/slate.h"

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_svd_r64()
{
    print_func( mpi_rank );

    // TODO: failing if m, n not divisible by nb?
    int64_t m=2000, n=1000, nb=100, p=2, q=2;
    assert( mpi_size == p*q );
    int64_t min_mn = MIN( m, n );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    random_matrix_type_r64( A );
    double* Sigma = (double*)malloc(min_mn * sizeof(double));

    // A = U Sigma V^H, singular values only
    slate_svd_vals_r64( A, Sigma, 0, NULL );

    slate_Matrix_destroy_r64( A );
    free(Sigma);
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_svd_r64();

    err = MPI_Finalize();
    assert( err == 0 );
}
