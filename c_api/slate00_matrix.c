// slate00_matrix.c
// create 2000 x 1000 matrix on 2 x 2 MPI process grid
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_matrix_r32()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    for (int64_t j = 0; j < slate_Matrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_r32( A ); ++i) {
            if (slate_Matrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T = slate_Matrix_at_r32( A, i, j );
                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );
            }
        }
    }
    slate_Matrix_destroy_r32( A );
}

//------------------------------------------------------------------------------
void test_matrix_r64()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    for (int64_t j = 0; j < slate_Matrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_r64( A ); ++i) {
            if (slate_Matrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T = slate_Matrix_at_r64( A, i, j );
                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );
            }
        }
    }
    slate_Matrix_destroy_r64( A );
}

//------------------------------------------------------------------------------
void test_matrix_c32()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    for (int64_t j = 0; j < slate_Matrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_c32( A ); ++i) {
            if (slate_Matrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T = slate_Matrix_at_c32( A, i, j );
                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );
            }
        }
    }
    slate_Matrix_destroy_c32( A );
}

//------------------------------------------------------------------------------
void test_matrix_c64()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    for (int64_t j = 0; j < slate_Matrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_c64( A ); ++i) {
            if (slate_Matrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T = slate_Matrix_at_c64( A, i, j );
                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );
            }
        }
    }
    slate_Matrix_destroy_c64( A );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_matrix_r32();
    test_matrix_r64();
    test_matrix_c32();
    test_matrix_c64();

    err = MPI_Finalize();
    assert( err == 0 );
}
