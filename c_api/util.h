#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

// macOS provides CMPLX only for clang, oddly.
#ifndef CMPLX
    #define CMPLX( re, im ) (re + im*_Complex_I)
#endif
#ifndef CMPLXF
    #define CMPLXF( re, im ) (re + im*_Complex_I)
#endif

#define MAX( x, y ) ( ( ( x ) > ( y ) ) ? ( x ) : ( y ) )
#define MIN( x, y ) ( ( ( x ) < ( y ) ) ? ( x ) : ( y ) )

//------------------------------------------------------------------------------
void print_func_( int rank, const char* func )
{
    printf( "rank %d: %s\n", rank, func );
}

#ifdef __GNUC__
    #define print_func( rank ) print_func_( rank, __PRETTY_FUNCTION__ )
#else
    #define print_func( rank ) print_func_( rank, __func__ )
#endif

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_r32( int64_t m, int64_t n, float* A, int64_t lda )
{
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            A[ i + j*lda ] = rand() / (float) RAND_MAX;
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_r64( int64_t m, int64_t n, double* A, int64_t lda )
{
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            A[ i + j*lda ] = rand() / (double) RAND_MAX;
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_c32( int64_t m, int64_t n, float _Complex* A, int64_t lda )
{
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            float complex z = CMPLXF( rand() / (float) RAND_MAX,
                                      rand() / (float) RAND_MAX );
            A[ i + j*lda ] = z;
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_c64( int64_t m, int64_t n, double _Complex* A, int64_t lda )
{
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            double complex z = CMPLX( rand() / (double) RAND_MAX,
                                      rand() / (double) RAND_MAX );
            A[ i + j*lda ] = z;
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_matrix_diag_dominant_r32(
    int64_t m, int64_t n, float* A, int64_t lda )
{
    int64_t max_mn = MAX( m, n );
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            A[ i + j*lda ] = rand() / (float) RAND_MAX;
        }
        if (j < m) {
            // make diagonal real & dominant
            A[ j + j*lda ] = A[ j + j*lda ] + max_mn;
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_matrix_diag_dominant_r64(
    int64_t m, int64_t n, double* A, int64_t lda )
{
    int64_t max_mn = MAX( m, n );
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            A[ i + j*lda ] = rand() / (double) RAND_MAX;
        }
        if (j < m) {
            // make diagonal real & dominant
            A[ j + j*lda ] = A[ j + j*lda ] + max_mn;
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_matrix_diag_dominant_c32(
    int64_t m, int64_t n, float _Complex* A, int64_t lda )
{
    int64_t max_mn = MAX( m, n );
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            float complex z = CMPLX( rand() / (float) RAND_MAX,
                                     rand() / (float) RAND_MAX );
            A[ i + j*lda ] = z;
        }
        if (j < m) {
            // make diagonal real & dominant
            A[ j + j*lda ] = creal( A[ j + j*lda ] ) + max_mn;
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_matrix_diag_dominant_c64(
    int64_t m, int64_t n, double _Complex* A, int64_t lda )
{
    int64_t max_mn = MAX( m, n );
    for (int64_t j = 0; j < n; ++j) {
        for (int64_t i = 0; i < m; ++i) {
            double complex z = CMPLX( rand() / (double) RAND_MAX,
                                      rand() / (double) RAND_MAX );
            A[ i + j*lda ] = z;
        }
        if (j < m) {
            // make diagonal real & dominant
            A[ j + j*lda ] = creal( A[ j + j*lda ] ) + max_mn;
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_type_r32( slate_Matrix_r32 A )
{
    for (int64_t j = 0; j < slate_Matrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_r32( A ); ++i) {
            if (slate_Matrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T = slate_Matrix_at_r32( A, i, j );
                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_type_r64( slate_Matrix_r64 A )
{
    for (int64_t j = 0; j < slate_Matrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_r64( A ); ++i) {
            if (slate_Matrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T = slate_Matrix_at_r64( A, i, j );
                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_type_c32( slate_Matrix_c32 A )
{
    for (int64_t j = 0; j < slate_Matrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_c32( A ); ++i) {
            if (slate_Matrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T = slate_Matrix_at_c32( A, i, j );
                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_matrix_type_c64( slate_Matrix_c64 A )
{
    for (int64_t j = 0; j < slate_Matrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_Matrix_mt_c64( A ); ++i) {
            if (slate_Matrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T = slate_Matrix_at_c64( A, i, j );
                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_symmetric_matrix_type_r32(
    slate_SymmetricMatrix_r32 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_SymmetricMatrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_SymmetricMatrix_mt_r32( A ); ++i) {
            if (slate_SymmetricMatrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_SymmetricMatrix_at_r32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_SymmetricMatrix_at_r32(A, i, j);
                else
                    continue;
                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_symmetric_matrix_type_r64(
    slate_SymmetricMatrix_r64 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_SymmetricMatrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_SymmetricMatrix_mt_r64( A ); ++i) {
            if (slate_SymmetricMatrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_SymmetricMatrix_at_r64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_SymmetricMatrix_at_r64(A, i, j);
                else
                    continue;
                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_symmetric_matrix_type_c32(
    slate_SymmetricMatrix_c32 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_SymmetricMatrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_SymmetricMatrix_mt_c32( A ); ++i) {
            if (slate_SymmetricMatrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_SymmetricMatrix_at_c32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_SymmetricMatrix_at_c32(A, i, j);
                else
                    continue;
                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_symmetric_matrix_type_c64(
    slate_SymmetricMatrix_c64 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_SymmetricMatrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_SymmetricMatrix_mt_c64( A ); ++i) {
            if (slate_SymmetricMatrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_SymmetricMatrix_at_c64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_SymmetricMatrix_at_c64(A, i, j);
                else
                    continue;
                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_hermitian_matrix_type_r32(
    slate_HermitianMatrix_r32 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_r32( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_r32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_r32(A, i, j);
                else
                    continue;
                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_hermitian_matrix_type_r64(
    slate_HermitianMatrix_r64 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_r64( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_r64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_r64(A, i, j);
                else
                    continue;
                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_hermitian_matrix_type_c32(
    slate_HermitianMatrix_c32 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_c32( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_c32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_c32(A, i, j);
                else
                    continue;
                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_hermitian_matrix_type_c64(
    slate_HermitianMatrix_c64 A, slate_Uplo uplo )
{
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_c64( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_c64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_c64(A, i, j);
                else
                    continue;
                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_triangular_matrix_type_r32(
    slate_TriangularMatrix_r32 A, slate_Uplo uplo, slate_Diag diag )
{
    for (int64_t j = 0; j < slate_TriangularMatrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_TriangularMatrix_mt_r32( A ); ++i) {
            if (slate_TriangularMatrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_TriangularMatrix_at_r32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_TriangularMatrix_at_r32(A, i, j);
                else
                    continue;

                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_triangular_matrix_type_r64(
    slate_TriangularMatrix_r64 A, slate_Uplo uplo, slate_Diag diag )
{
    for (int64_t j = 0; j < slate_TriangularMatrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_TriangularMatrix_mt_r64( A ); ++i) {
            if (slate_TriangularMatrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_TriangularMatrix_at_r64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_TriangularMatrix_at_r64(A, i, j);
                else
                    continue;

                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_triangular_matrix_type_c32(
    slate_TriangularMatrix_c32 A, slate_Uplo uplo, slate_Diag diag )
{
    for (int64_t j = 0; j < slate_TriangularMatrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_TriangularMatrix_mt_c32( A ); ++i) {
            if (slate_TriangularMatrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_TriangularMatrix_at_c32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_TriangularMatrix_at_c32(A, i, j);
                else
                    continue;

                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random matrix A
void random_triangular_matrix_type_c64(
    slate_TriangularMatrix_c64 A, slate_Uplo uplo, slate_Diag diag )
{
    for (int64_t j = 0; j < slate_TriangularMatrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_TriangularMatrix_mt_c64( A ); ++i) {
            if (slate_TriangularMatrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_TriangularMatrix_at_c64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_TriangularMatrix_at_c64(A, i, j);
                else
                    continue;

                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_hermitian_matrix_type_diag_dominant_r32(
    slate_HermitianMatrix_r32 A, slate_Uplo uplo )
{
    int64_t max_mn = MAX( slate_HermitianMatrix_m_r32( A ),
                          slate_HermitianMatrix_n_r32( A ) );
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_r32( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_r32( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_r32( A, i, j )) {
                slate_Tile_r32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_r32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_r32(A, i, j);
                else
                    continue;

                random_matrix_r32( slate_Tile_mb_r32( T ),
                                   slate_Tile_nb_r32( T ),
                                   slate_Tile_data_r32( T ),
                                   slate_Tile_stride_r32( T ) );

                if (i == j) {
                    // assuming tileMb == tileNb, then i == j are diagonal tiles
                    // make diagonal real & dominant
                    int64_t min_mb_nb = MIN(
                              slate_Tile_mb_r32( T ), slate_Tile_nb_r32( T ) );
                    for (int64_t ii = 0; ii < min_mb_nb; ++ii) {
                        float* data = slate_Tile_data_r32( T );
                        data[ ii + ii*slate_Tile_stride_r32( T ) ] =
                          creal( data[ ii + ii*slate_Tile_stride_r32( T ) ] ) +
                          max_mn;
                    }
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_hermitian_matrix_type_diag_dominant_r64(
    slate_HermitianMatrix_r64 A, slate_Uplo uplo )
{
    int64_t max_mn = MAX( slate_HermitianMatrix_m_r64( A ),
                          slate_HermitianMatrix_n_r64( A ) );
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_r64( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_r64( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_r64( A, i, j )) {
                slate_Tile_r64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_r64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_r64(A, i, j);
                else
                    continue;

                random_matrix_r64( slate_Tile_mb_r64( T ),
                                   slate_Tile_nb_r64( T ),
                                   slate_Tile_data_r64( T ),
                                   slate_Tile_stride_r64( T ) );

                if (i == j) {
                    // assuming tileMb == tileNb, then i == j are diagonal tiles
                    // make diagonal real & dominant
                    int64_t min_mb_nb = MIN(
                              slate_Tile_mb_r64( T ), slate_Tile_nb_r64( T ) );
                    for (int64_t ii = 0; ii < min_mb_nb; ++ii) {
                        double* data = slate_Tile_data_r64( T );
                        data[ ii + ii*slate_Tile_stride_r64( T ) ] =
                          creal( data[ ii + ii*slate_Tile_stride_r64( T ) ] ) +
                          max_mn;
                    }
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_hermitian_matrix_type_diag_dominant_c32(
    slate_HermitianMatrix_c32 A, slate_Uplo uplo )
{
    int64_t max_mn = MAX( slate_HermitianMatrix_m_c32( A ),
                          slate_HermitianMatrix_n_c32( A ) );
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_c32( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_c32( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_c32( A, i, j )) {
                slate_Tile_c32 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_c32(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_c32(A, i, j);
                else
                    continue;

                random_matrix_c32( slate_Tile_mb_c32( T ),
                                   slate_Tile_nb_c32( T ),
                                   slate_Tile_data_c32( T ),
                                   slate_Tile_stride_c32( T ) );

                if (i == j) {
                    // assuming tileMb == tileNb, then i == j are diagonal tiles
                    // make diagonal real & dominant
                    int64_t min_mb_nb = MIN(
                              slate_Tile_mb_c32( T ), slate_Tile_nb_c32( T ) );
                    for (int64_t ii = 0; ii < min_mb_nb; ++ii) {
                        float _Complex* data = slate_Tile_data_c32( T );
                        data[ ii + ii*slate_Tile_stride_c32( T ) ] =
                          creal( data[ ii + ii*slate_Tile_stride_c32( T ) ] ) +
                          max_mn;
                    }
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// generate random, diagonally dominant matrix A
void random_hermitian_matrix_type_diag_dominant_c64(
    slate_HermitianMatrix_c64 A, slate_Uplo uplo )
{
    int64_t max_mn = MAX( slate_HermitianMatrix_m_c64( A ),
                          slate_HermitianMatrix_n_c64( A ) );
    for (int64_t j = 0; j < slate_HermitianMatrix_nt_c64( A ); ++j) {
        for (int64_t i = 0; i < slate_HermitianMatrix_mt_c64( A ); ++i) {
            if (slate_HermitianMatrix_tileIsLocal_c64( A, i, j )) {
                slate_Tile_c64 T;
                if (uplo == slate_Uplo_Upper && i <= j)
                    T = slate_HermitianMatrix_at_c64(A, i, j);
                else if (uplo == slate_Uplo_Lower && i >= j)
                    T = slate_HermitianMatrix_at_c64(A, i, j);
                else
                    continue;

                random_matrix_c64( slate_Tile_mb_c64( T ),
                                   slate_Tile_nb_c64( T ),
                                   slate_Tile_data_c64( T ),
                                   slate_Tile_stride_c64( T ) );

                if (i == j) {
                    // assuming tileMb == tileNb, then i == j are diagonal tiles
                    // make diagonal real & dominant
                    int64_t min_mb_nb = MIN(
                              slate_Tile_mb_c64( T ), slate_Tile_nb_c64( T ) );
                    for (int64_t ii = 0; ii < min_mb_nb; ++ii) {
                        double _Complex* data = slate_Tile_data_c64( T );
                        data[ ii + ii*slate_Tile_stride_c64( T ) ] =
                          creal( data[ ii + ii*slate_Tile_stride_c64( T ) ] ) +
                          max_mn;
                    }
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
void print_matrix_r32( const char* label, int m, int n, float* A, int lda )
{
    printf( "%s = [\n", label );
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf( "  %7.4f", A[ i + j*lda ] );
        }
        printf( "\n" );
    }
    printf( "];\n" );
}

//------------------------------------------------------------------------------
void print_matrix_r64( const char* label, int m, int n, double* A, int lda )
{
    printf( "%s = [\n", label );
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf( "  %7.4lf", A[ i + j*lda ] );
        }
        printf( "\n" );
    }
    printf( "];\n" );
}

//------------------------------------------------------------------------------
void print_matrix_c32(
    const char* label, int m, int n, float _Complex* A, int lda )
{
    printf( "%s = [\n", label );
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf( "  %7.4f + %7.4fi",
                    creal(A[ i + j*lda ]), cimag(A[ i + j*lda ]) );
        }
        printf( "\n" );
    }
    printf( "];\n" );
}


//------------------------------------------------------------------------------
void print_matrix_c64(
    const char* label, int m, int n, double _Complex* A, int lda )
{
    printf( "%s = [\n", label );
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf( "  %7.4lf + %7.4fi",
                    creal(A[ i + j*lda ]), cimag(A[ i + j*lda ]) );
        }
        printf( "\n" );
    }
    printf( "];\n" );
}

//------------------------------------------------------------------------------
// suppress compiler "unused" warning for variable x
#define unused( x ) ((void) x)

#endif // UTIL_H
