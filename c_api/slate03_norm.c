// example_04.cc
// BLAS routines
#include <slate/c_api/slate.h>

#include "util.h"

int mpi_size = 0;
int mpi_rank = 0;

//------------------------------------------------------------------------------
void test_general_norm_r32()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r32 A = slate_Matrix_create_r32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r32( A );
    random_matrix_type_r32( A );

    double A_norm;

    A_norm = slate_norm_r32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_Matrix_destroy_r32( A );
}

//------------------------------------------------------------------------------
void test_general_norm_r64()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_r64 A = slate_Matrix_create_r64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_r64( A );
    random_matrix_type_r64( A );

    double A_norm;

    A_norm = slate_norm_r64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_r64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_Matrix_destroy_r64( A );
}

//------------------------------------------------------------------------------
void test_general_norm_c32()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c32 A = slate_Matrix_create_c32(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c32( A );
    random_matrix_type_c32( A );

    double A_norm;

    A_norm = slate_norm_c32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_Matrix_destroy_c32( A );
}

//------------------------------------------------------------------------------
void test_general_norm_c64()
{
    print_func( mpi_rank );

    int64_t m=2000, n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_Matrix_c64 A = slate_Matrix_create_c64(
        m, n, nb, p, q, MPI_COMM_WORLD );
    slate_Matrix_insertLocalTiles_c64( A );
    random_matrix_type_c64( A );

    double A_norm;

    A_norm = slate_norm_c64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_norm_c64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_Matrix_destroy_c64( A );
}

//------------------------------------------------------------------------------
void test_symmetric_norm_r32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_r32 A = slate_SymmetricMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_r32( A );
    random_symmetric_matrix_type_r32( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_symmetric_norm_r32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_SymmetricMatrix_destroy_r32( A );
}

//------------------------------------------------------------------------------
void test_symmetric_norm_r64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_r64 A = slate_SymmetricMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_r64( A );
    random_symmetric_matrix_type_r64( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_symmetric_norm_r64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_r64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_SymmetricMatrix_destroy_r64( A );
}

//------------------------------------------------------------------------------
void test_symmetric_norm_c32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_c32 A = slate_SymmetricMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_c32( A );
    random_symmetric_matrix_type_c32( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_symmetric_norm_c32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_SymmetricMatrix_destroy_c32( A );
}

//------------------------------------------------------------------------------
void test_symmetric_norm_c64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_SymmetricMatrix_c64 A = slate_SymmetricMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_SymmetricMatrix_insertLocalTiles_c64( A );
    random_symmetric_matrix_type_c64( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_symmetric_norm_c64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_symmetric_norm_c64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_SymmetricMatrix_destroy_c64( A );
}

//------------------------------------------------------------------------------
void test_hermitian_norm_r32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r32 A = slate_HermitianMatrix_create_r32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r32( A );
    random_hermitian_matrix_type_r32( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_hermitian_norm_r32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_HermitianMatrix_destroy_r32( A );
}

//------------------------------------------------------------------------------
void test_hermitian_norm_r64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_r64 A = slate_HermitianMatrix_create_r64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_r64( A );
    random_hermitian_matrix_type_r64( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_hermitian_norm_r64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_r64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_HermitianMatrix_destroy_r64( A );
}

//------------------------------------------------------------------------------
void test_hermitian_norm_c32()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c32 A = slate_HermitianMatrix_create_c32(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c32( A );
    random_hermitian_matrix_type_c32( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_hermitian_norm_c32( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c32( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c32( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c32( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_HermitianMatrix_destroy_c32( A );
}

//------------------------------------------------------------------------------
void test_hermitian_norm_c64()
{
    print_func( mpi_rank );

    int64_t n=1000, nb=256, p=2, q=2;
    assert( mpi_size == p*q );
    slate_HermitianMatrix_c64 A = slate_HermitianMatrix_create_c64(
        slate_Uplo_Lower, n, nb, p, q, MPI_COMM_WORLD );
    slate_HermitianMatrix_insertLocalTiles_c64( A );
    random_hermitian_matrix_type_c64( A, slate_Uplo_Lower );

    double A_norm;

    A_norm = slate_hermitian_norm_c64( slate_Norm_One, A, 0, NULL );
    printf( "rank %d: one norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c64( slate_Norm_Inf, A, 0, NULL );
    printf( "rank %d: inf norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c64( slate_Norm_Max, A, 0, NULL );
    printf( "rank %d: max norm %.6f\n", mpi_rank, A_norm );

    A_norm = slate_hermitian_norm_c64( slate_Norm_Fro, A, 0, NULL );
    printf( "rank %d: fro norm %.6f\n", mpi_rank, A_norm );

    slate_HermitianMatrix_destroy_c64( A );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    int provided = 0;
    int err = MPI_Init_thread( &argc, &argv, MPI_THREAD_MULTIPLE, &provided );
    assert( err == 0 );
    assert( provided == MPI_THREAD_MULTIPLE );

    err = MPI_Comm_size( MPI_COMM_WORLD, &mpi_size );
    assert( err == 0 );
    if (mpi_size != 4) {
        printf( "Usage: mpirun -np 4 %s  # 4 ranks hard coded\n", argv[0] );
        return -1;
    }

    err = MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );
    assert( err == 0 );

    // so random_matrix is different on different ranks.
    srand( 100 * mpi_rank );

    test_general_norm_r32();
    test_general_norm_r64();
    test_general_norm_c32();
    test_general_norm_c64();

    test_symmetric_norm_r32();
    test_symmetric_norm_r64();
    test_symmetric_norm_c32();
    test_symmetric_norm_c64();

    test_hermitian_norm_r32();
    test_hermitian_norm_r64();
    test_hermitian_norm_c32();
    test_hermitian_norm_c64();

    // test_triangular_norm_r32(); // todo
    // test_triangular_norm_r64(); // todo
    // test_triangular_norm_c32(); // todo
    // test_triangular_norm_c64(); // todo

    err = MPI_Finalize();
    assert( err == 0 );
}
