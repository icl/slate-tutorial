! slate05a_linear_system_lu.f90
! Solve AX = B using LU factorization
program slate05a_linear_system_lu
    use, intrinsic :: iso_fortran_env
    use slate
    use mpi
    implicit none

    !! Constants
    integer(kind=c_int64_t), parameter :: m    = 2000
    integer(kind=c_int64_t), parameter :: n    = 1000
    integer(kind=c_int64_t), parameter :: nrhs = 100
    integer(kind=c_int64_t), parameter :: nb   = 100

    integer(kind=c_int),     parameter :: p = 2
    integer(kind=c_int),     parameter :: q = 2

    !! Variables
    character(len=64)                  :: arg

    integer(kind=c_int)                :: provided
    integer(kind=c_int)                :: ierr
    integer(kind=c_int)                :: mpi_rank
    integer(kind=c_int)                :: mpi_size
    integer(kind=c_int)                :: myrow
    integer(kind=c_int)                :: mycol

    integer(kind=c_int64_t)            :: mlocA, nlocA, lldA
    integer(kind=c_int64_t)            :: i, max_mn, i1, j1

    type(c_ptr)                        :: A, B, X, BX, opts
    real(kind=c_float),      pointer   :: A_data(:)

    !! MPI
    call MPI_Init_thread( MPI_THREAD_MULTIPLE, provided, ierr )
    if ((ierr .ne. 0) .or. (provided .ne. MPI_THREAD_MULTIPLE)) then
        print *, "Error: MPI_Init_thread"
        return
    end if
    call MPI_Comm_size( MPI_COMM_WORLD, mpi_size, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_size"
        return
    end if
    if (mpi_size .ne. 4) then
        CALL getarg( 0, arg )
        write( *, '(a, a, a)' ) &
               "Usage: mpirun -np 4 ", trim(arg), " # 4 ranks hard coded"
        return;
    end if
    if (mpi_size .ne. p*q) then
        print *, "Error: mpi_size not equal (p x q)"
        return
    end if
    call MPI_Comm_rank( MPI_COMM_WORLD, mpi_rank, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_rank"
        return
    end if

    myrow = whoismyrow( mpi_rank, p )
    mycol = whoismycol( mpi_rank, q )
    call srand( 100 * mpi_rank )

    mlocA = localRowsCols( m, nb, myrow, p )
    nlocA = localRowsCols( n, nb, mycol, q )
    lldA = mlocA
    allocate( A_data( lldA*nlocA ) )
    do i = 1, lldA*nlocA
        A_data( i ) = rand()
    end do
    A = slate_Matrix_create_fromScaLAPACK_r32( &
        m, n, A_data, lldA, nb, nb, p, q, MPI_COMM_WORLD )

    max_mn = max( m, n )
    BX = slate_Matrix_create_r32( max_mn, nrhs, nb, p, q, MPI_COMM_WORLD )
    call slate_Matrix_insertLocalTiles_r32( BX )

    i1 = 0
    j1 = 0
    B = slate_Matrix_create_slice_r32( BX, i1, m-1, j1, nrhs-1 );
    X = slate_Matrix_create_slice_r32( BX, i1, n-1, j1, nrhs-1 );

    call slate_least_squares_solve_r32( A, BX, 0, opts );

    call slate_Matrix_destroy_r32( A )
    deallocate( A_data )

    call slate_Matrix_destroy_r32( BX )
    call slate_Matrix_destroy_r32( B )
    call slate_Matrix_destroy_r32( X )

    call MPI_Finalize( ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Finalize"
        return
    end if

contains

    function localRowsCols( n, nb, iproc, mpi_size ) result( num )
        use, intrinsic :: iso_fortran_env
        use slate
        use mpi
        implicit none

        integer(kind=c_int64_t)  :: n
        integer(kind=c_int64_t)  :: nb
        integer(kind=c_int64_t)  :: nblocks
        integer(kind=c_int64_t)  :: num
        integer(kind=c_int64_t)  :: extra_blocks
        integer(kind=c_int)      :: iproc
        integer(kind=c_int)      :: mpi_size

        nblocks      = n / nb
        num          = (nblocks / mpi_size) * nb
        extra_blocks = mod( nblocks, mpi_size)

        if (iproc < extra_blocks) then
            num = num + nb
        else if (iproc == extra_blocks) then
            num = num + mod( n, nb )
        end if
    end function

    function whoismyrow( mpi_rank, p ) result( myrow )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: p
        integer(kind=c_int) :: myrow

        myrow = mod( mpi_rank, p )
    end function

    function whoismycol( mpi_rank, q ) result( mycol )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: q
        integer(kind=c_int) :: mycol

        mycol = mpi_rank / q
    end function

end program slate05a_linear_system_lu
