! slate00_matrix.f90
! create 2000 x 1000 matrix on 2 x 2 MPI process grid
program slate00_matrix
    use, intrinsic :: iso_fortran_env
    use slate
    use mpi
    implicit none

    !! Constants
    integer(kind=c_int64_t), parameter :: m  = 2000
    integer(kind=c_int64_t), parameter :: n  = 1000
    integer(kind=c_int64_t), parameter :: nb = 256

    integer(kind=c_int),     parameter :: p = 2
    integer(kind=c_int),     parameter :: q = 2

    !! Variables
    character(len=64)                  :: arg

    integer(kind=c_int)                :: provided
    integer(kind=c_int)                :: ierr
    integer(kind=c_int)                :: mpi_rank
    integer(kind=c_int)                :: mpi_size

    type(c_ptr)                        :: A, tile_data_ptr
    type(slate_Tile_r32)               :: T

    integer(kind=c_int64_t)            :: i, j, ii, jj
    integer(kind=c_int64_t)            :: tile_nb, tile_mb, tile_lda

    real(kind=c_float),      pointer   :: tile_data(:)

    !! MPI
    call MPI_Init_thread( MPI_THREAD_MULTIPLE, provided, ierr )
    if ((ierr .ne. 0) .or. (provided .ne. MPI_THREAD_MULTIPLE)) then
        print *, "Error: MPI_Init_thread"
        return
    end if
    call MPI_Comm_size( MPI_COMM_WORLD, mpi_size, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_size"
        return
    end if
    if (mpi_size .ne. 4) then
        CALL getarg( 0, arg )
        write( *, '(a, a, a)' ) &
               "Usage: mpirun -np 4 ", trim(arg), " # 4 ranks hard coded"
        return;
    end if
    if (mpi_size .ne. p*q) then
        print *, "Error: mpi_size not equal (p x q)"
        return
    end if
    call MPI_Comm_rank( MPI_COMM_WORLD, mpi_rank, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_rank"
        return
    end if

    call srand( 100 * mpi_rank )

    A = slate_Matrix_create_r32( m, n, nb, p, q, MPI_COMM_WORLD )
    call slate_Matrix_insertLocalTiles_r32( A )
    do j = 0, slate_Matrix_nt_r32( A )-1
        do i = 0, slate_Matrix_mt_r32( A )-1
            if (slate_Matrix_tileIsLocal_r32( A, i, j )) then
                T = slate_Matrix_at_r32( A, i, j )
                tile_nb = slate_Tile_nb_r32( T )
                tile_mb = slate_Tile_mb_r32( T )
                tile_lda = slate_Tile_stride_r32( T )
                tile_data_ptr = slate_Tile_data_r32( T )
                CALL c_f_pointer( tile_data_ptr, tile_data, [tile_mb*tile_nb] )
                do jj = 1, tile_nb-1
                    do ii = 1, tile_mb-1
                        tile_data( ii + jj*tile_lda ) = rand()
                    end do
                end do
            end if
        end do
    end do

    call slate_Matrix_destroy_r32( A );

    call MPI_Finalize( ierr );
    if (ierr .ne. 0) then
        print *, "Error: MPI_Finalize"
        return
    end if

end program slate00_matrix
