! slate05a_linear_system_cholesky.f90
! Solve AX = B using Cholesky factorization
program slate05a_linear_system_cholesky
    use, intrinsic :: iso_fortran_env
    use slate
    use mpi
    implicit none

    !! Constants
    integer(kind=c_int64_t), parameter :: n    = 1000
    integer(kind=c_int64_t), parameter :: nrhs = 100
    integer(kind=c_int64_t), parameter :: nb   = 256

    integer(kind=c_int),     parameter :: p = 2
    integer(kind=c_int),     parameter :: q = 2

    !! Variables
    character(len=64)                  :: arg

    integer(kind=c_int)                :: provided
    integer(kind=c_int)                :: ierr
    integer(kind=c_int)                :: mpi_rank
    integer(kind=c_int)                :: mpi_size
    integer(kind=c_int)                :: myrow
    integer(kind=c_int)                :: mycol

    integer(kind=c_int64_t)            :: mlocA, nlocA, lldA
    integer(kind=c_int64_t)            :: mlocB, nlocB, lldB

    integer(kind=c_int64_t)            :: i

    type(c_ptr)                        :: A, B, opts
    real(kind=c_float),      pointer   :: A_data(:), B_data(:)

    !! MPI
    call MPI_Init_thread( MPI_THREAD_MULTIPLE, provided, ierr )
    if ((ierr .ne. 0) .or. (provided .ne. MPI_THREAD_MULTIPLE)) then
        print *, "Error: MPI_Init_thread"
        return
    end if
    call MPI_Comm_size( MPI_COMM_WORLD, mpi_size, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_size"
        return
    end if
    if (mpi_size .ne. 4) then
        CALL getarg( 0, arg )
        write( *, '(a, a, a)' ) &
               "Usage: mpirun -np 4 ", trim(arg), " # 4 ranks hard coded"
        return;
    end if
    if (mpi_size .ne. p*q) then
        print *, "Error: mpi_size not equal (p x q)"
        return
    end if
    call MPI_Comm_rank( MPI_COMM_WORLD, mpi_rank, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_rank"
        return
    end if

    myrow = whoismyrow( mpi_rank, p )
    mycol = whoismycol( mpi_rank, q )
    call srand( 100 * mpi_rank )

    mlocA = localRowsCols( n, nb, myrow, p )
    nlocA = localRowsCols( n, nb, mycol, q )
    lldA = mlocA
    allocate( A_data( lldA*nlocA ) )
    do i = 1, lldA*nlocA
        A_data( i ) = rand()
    end do
    A = slate_HermitianMatrix_create_fromScaLAPACK_r32( &
        'L', n, A_data, lldA, nb, p, q, MPI_COMM_WORLD )

    mlocB = localRowsCols( n, nb, myrow, p )
    nlocB = localRowsCols( nrhs, nb, mycol, q )
    lldB = mlocB
    allocate( B_data( lldB*nlocB ) )
    do i = 1, lldB*nlocB
        B_data( i ) = rand()
    end do
    B = slate_Matrix_create_fromScaLAPACK_r32( &
        n, nrhs, B_data, lldB, nb, nb, p, q, MPI_COMM_WORLD )

    call slate_chol_solve_r32( A, B, 0, opts );

    call slate_HermitianMatrix_destroy_r32( A )
    deallocate( A_data )
    call slate_Matrix_destroy_r32( B )
    deallocate( B_data )

    allocate( A_data( lldA*nlocA ) )
    do i = 1, lldA*nlocA
        A_data( i ) = rand()
    end do
    A = slate_HermitianMatrix_create_fromScaLAPACK_r32( &
        'L', n, A_data, lldA, nb, p, q, MPI_COMM_WORLD )

    call slate_chol_factor_r32( A, 0, opts )
    call slate_chol_inverse_using_factor_r32( A, 0, opts )

    call slate_HermitianMatrix_destroy_r32( A )
    deallocate( A_data )

    call MPI_Finalize( ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Finalize"
        return
    end if

contains

    function localRowsCols( n, nb, iproc, mpi_size ) result( num )
        use, intrinsic :: iso_fortran_env
        use slate
        use mpi
        implicit none

        integer(kind=c_int64_t)  :: n
        integer(kind=c_int64_t)  :: nb
        integer(kind=c_int64_t)  :: nblocks
        integer(kind=c_int64_t)  :: num
        integer(kind=c_int64_t)  :: extra_blocks
        integer(kind=c_int)      :: iproc
        integer(kind=c_int)      :: mpi_size

        nblocks      = n / nb
        num          = (nblocks / mpi_size) * nb
        extra_blocks = mod( nblocks, mpi_size)

        if (iproc < extra_blocks) then
            num = num + nb
        else if (iproc == extra_blocks) then
            num = num + mod( n, nb )
        end if
    end function

    function whoismyrow( mpi_rank, p ) result( myrow )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: p
        integer(kind=c_int) :: myrow

        myrow = mod( mpi_rank, p )
    end function

    function whoismycol( mpi_rank, q ) result( mycol )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: q
        integer(kind=c_int) :: mycol

        mycol = mpi_rank / q
    end function

end program slate05a_linear_system_cholesky
