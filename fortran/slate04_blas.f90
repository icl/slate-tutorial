! example_04.f90
! BLAS routines
program slate04_blas
    use, intrinsic :: iso_fortran_env
    use slate
    use mpi
    implicit none

    !! Constants
    integer(kind=c_int64_t), parameter :: m  = 2000
    integer(kind=c_int64_t), parameter :: n  = 1000
    integer(kind=c_int64_t), parameter :: k  = 500
    integer(kind=c_int64_t), parameter :: nb = 256

    integer(kind=c_int),     parameter :: p = 2
    integer(kind=c_int),     parameter :: q = 2

    real(kind=c_float),      parameter :: alpha = 2.0
    real(kind=c_float),      parameter :: beta  = 1.0

    !! Variables
    character(len=64)                  :: arg

    integer(kind=c_int)                :: provided
    integer(kind=c_int)                :: ierr
    integer(kind=c_int)                :: mpi_rank
    integer(kind=c_int)                :: mpi_size
    integer(kind=c_int)                :: myrow
    integer(kind=c_int)                :: mycol

    integer(kind=c_int64_t)            :: mlocA, nlocA, lldA
    integer(kind=c_int64_t)            :: mlocB, nlocB, lldB
    integer(kind=c_int64_t)            :: mlocC, nlocC, lldC
    integer(kind=c_int64_t)            :: i

    type(c_ptr)                        :: A, B, C, opts
    real(kind=c_float),      pointer   :: A_data(:), B_data(:), C_data(:)

    !! MPI
    call MPI_Init_thread( MPI_THREAD_MULTIPLE, provided, ierr )
    if ((ierr .ne. 0) .or. (provided .ne. MPI_THREAD_MULTIPLE)) then
        print *, "Error: MPI_Init_thread"
        return
    end if
    call MPI_Comm_size( MPI_COMM_WORLD, mpi_size, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_size"
        return
    end if
    if (mpi_size .ne. 4) then
        CALL getarg( 0, arg )
        write( *, '(a, a, a)' ) &
               "Usage: mpirun -np 4 ", trim(arg), " # 4 ranks hard coded"
        return;
    end if
    if (mpi_size .ne. p*q) then
        print *, "Error: mpi_size not equal (p x q)"
        return
    end if
    call MPI_Comm_rank( MPI_COMM_WORLD, mpi_rank, ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Comm_rank"
        return
    end if

    myrow = whoismyrow( mpi_rank, p )
    mycol = whoismycol( mpi_rank, q )
    call srand( 100 * mpi_rank )

    mlocA = localRowsCols( m, nb, myrow, p )
    nlocA = localRowsCols( k, nb, mycol, q )
    lldA = mlocA
    allocate( A_data( lldA*nlocA ) )
    do i = 1, lldA*nlocA
        A_data( i ) = rand()
    end do
    A = slate_Matrix_create_fromScaLAPACK_r32( &
        m, k, A_data, lldA, nb, nb, p, q, MPI_COMM_WORLD )

    mlocB = localRowsCols( k, nb, myrow, p )
    nlocB = localRowsCols( n, nb, mycol, q )
    lldB = mlocB
    allocate( B_data( lldB*nlocB ) )
    do i = 1, lldB*nlocB
        B_data( i ) = rand()
    end do
    B = slate_Matrix_create_fromScaLAPACK_r32( &
        k, n, B_data, lldB, nb, nb, p, q, MPI_COMM_WORLD )

    mlocC = localRowsCols( m, nb, myrow, p )
    nlocC = localRowsCols( n, nb, mycol, q )
    lldC = mlocC
    allocate( C_data( lldC*nlocC ) )
    do i = 1, lldC*nlocC
        C_data( i ) = rand()
    end do
    C = slate_Matrix_create_fromScaLAPACK_r32( &
        m, n, C_data, lldC, nb, nb, p, q, MPI_COMM_WORLD )

    ! C = alpha A B + beta C
    call slate_multiply_r32( alpha, A, B, beta, C, 0, opts )

    call slate_Matrix_destroy_r32( A )
    deallocate( A_data )
    call slate_Matrix_destroy_r32( B )
    deallocate( B_data )
    call slate_Matrix_destroy_r32( C )
    deallocate( C_data )

    call MPI_Finalize( ierr )
    if (ierr .ne. 0) then
        print *, "Error: MPI_Finalize"
        return
    end if

contains

    function localRowsCols( n, nb, iproc, mpi_size ) result( num )
        use, intrinsic :: iso_fortran_env
        use slate
        use mpi
        implicit none

        integer(kind=c_int64_t)  :: n
        integer(kind=c_int64_t)  :: nb
        integer(kind=c_int64_t)  :: nblocks
        integer(kind=c_int64_t)  :: num
        integer(kind=c_int64_t)  :: extra_blocks
        integer(kind=c_int)      :: iproc
        integer(kind=c_int)      :: mpi_size

        nblocks      = n / nb
        num          = (nblocks / mpi_size) * nb
        extra_blocks = mod( nblocks, mpi_size)

        if (iproc < extra_blocks) then
            num = num + nb
        else if (iproc == extra_blocks) then
            num = num + mod( n, nb )
        end if
    end function

    function whoismyrow( mpi_rank, p ) result( myrow )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: p
        integer(kind=c_int) :: myrow

        myrow = mod( mpi_rank, p )
    end function

    function whoismycol( mpi_rank, q ) result( mycol )
        integer(kind=c_int) :: mpi_rank
        integer(kind=c_int) :: q
        integer(kind=c_int) :: mycol

        mycol = mpi_rank / q
    end function

end program slate04_blas
