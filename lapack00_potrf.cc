// example_00_lapackpp.cc
// LAPACK++
#include <lapack.hh>

#include "util.hh"

int verbose = 0;

//------------------------------------------------------------------------------
template <typename scalar_type>
void test_potrf()
{
    print_func( 0 );

    int64_t n=100, lda=n;
    scalar_type* A = new scalar_type[ lda * n ];  // n x n
    random_matrix_diag_dominant( n, n, A, lda );

    int64_t info = lapack::potrf( blas::Uplo::Lower, n, A, lda );
    assert( info == 0 );

    delete[] A;
}

//------------------------------------------------------------------------------
// Cholesky factorization, written using BLAS++ and LAPACK++.
// Assumes lower storage.
template <typename scalar_type>
int64_t cholesky( int n, scalar_type* A, int lda )
{
    #define A(i, j) (A + (i) + (j)*lda)  // pointer to A(i, j) entry

    const blas::real_type<scalar_type> r_one = 1;
    const scalar_type one = 1;
    int64_t info = 0;
    int64_t nb = 4;
    for (int64_t j = 0; j < n; j += nb) {
        int64_t jb = blas::min( nb, n - j );
        blas::herk( blas::Layout::ColMajor,
                    blas::Uplo::Lower, blas::Op::NoTrans, jb, j,
                    -r_one, A(j, 0), lda,
                     r_one, A(j, j), lda );
        info = lapack::potrf2( blas::Uplo::Lower, jb, A(j, j), lda );
        if (info != 0) {
            info += j;
            break;
        }
        if (j + jb < n) {
            blas::gemm( blas::Layout::ColMajor,
                        blas::Op::NoTrans, blas::Op::ConjTrans,
                        n - j - jb, jb, j,
                        -one, A(j+jb, 0), lda,
                              A(j,    0), lda,
                         one, A(j+jb, j), lda );
            blas::trsm( blas::Layout::ColMajor,
                        blas::Side::Right, blas::Uplo::Lower,
                        blas::Op::ConjTrans, blas::Diag::NonUnit,
                        n - j - jb, jb,
                        one, A(j,    j), lda,
                             A(j+jb, j), lda );
        }
    }
    return info;

    #undef A
}

//------------------------------------------------------------------------------
template <typename scalar_type>
void test_cholesky()
{
    print_func( 0 );

    int64_t n = 7, lda = n, info;

    // -----
    // double
    scalar_type Ad[] = {
        6, 1, 1, 1, 1, 1, 1,
        1, 6, 1, 1, 1, 1, 1,
        1, 1, 6, 1, 1, 1, 1,
        1, 1, 1, 6, 1, 1, 1,
        1, 1, 1, 1, 6, 1, 1,
        1, 1, 1, 1, 1, 6, 1,
        1, 1, 1, 1, 1, 1, 6,
    };

    if (verbose)
        print_matrix( "A", n, n, Ad, lda );

    info = cholesky( n, Ad, lda );
    assert( info == 0 );

    if (verbose)
        print_matrix( "L", n, n, Ad, lda );
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    test_potrf< float >();
    test_potrf< double >();
    test_potrf< std::complex<float> >();
    test_potrf< std::complex<double> >();

    test_cholesky< float >();
    test_cholesky< double >();
    test_cholesky< std::complex<float> >();
    test_cholesky< std::complex<double> >();

    return 0;
}
