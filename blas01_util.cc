// blas01_util.cc
// BLAS++ utilities: blas::real_type, blas::real, blas::imag, blas::is_complex
#include <blas.hh>

#include "util.hh"

//------------------------------------------------------------------------------
template <typename scalar_type>
void test_util( scalar_type alpha )
{
    print_func( 0 );

    int64_t n=100;
    scalar_type* x = new scalar_type[ n ];
    random_matrix( n, 1, x, n );

    using real_type = blas::real_type< scalar_type >;
    real_type norm = blas::nrm2( n, x, 1 );
    unused(norm);

    using blas::real;
    using blas::imag;
    if (blas::is_complex<scalar_type>::value)
        printf( "alpha %7.4f + %7.4fi\n", real(alpha), imag(alpha) );
    else
        printf( "alpha %7.4f\n", real(alpha) );

    delete[] x;
}

//------------------------------------------------------------------------------
int main( int argc, char** argv )
{
    test_util(  float(1.234) );
    test_util( double(2.468) );
    test_util( std::complex< float>( 3.1415, 0.5678 ) );
    test_util( std::complex<double>( 6.2830, 1.1356 ) );
}
