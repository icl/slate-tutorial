These are sample codes for SLATE, based on the SLATE tutorial presentation.
Slides are available at:

https://bitbucket.org/icl/slate/downloads/slate-tutorial.pdf

These examples are also referenced in the SLATE Users' Guide:

https://www.icl.utk.edu/publications/swan-010

1. Compile and install SLATE per its directions in slate/INSTALL.md. It
can be installed in your home directory if desired. Installation puts
the SLATE, BLAS++, and LAPACK++ headers all in the same include directory,
and libraries all in the same lib directory, which simplifies compiling.

    slate>  make

    # By default, installs into /opt/slate
    slate>  make install

    # Alternatively, install somewhere else like in your home directory.
    # (Syntax when using CMake is a bit different; see slate/INSTALL.md.)
    slate>  make install prefix=${HOME}/install

2. Switching to this SLATE examples directory, create a `make.inc` file
that sets `slate_dir` to where SLATE was installed. The scalapack
example also needs `scalapack_libs` set to your system's ScaLAPACK
libraries. For instance:

    # make.inc file
    slate_dir = ${HOME}/install
    scalapack_libs = -L/path/to/scalapack -lscalapack -lgfortran

With Intel MKL, `scalapack_libs` should be a variant of this:

    scalapack_libs = -L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64

depending on your MPI library (Intel MPI or Open MPI). Normally use the
`lp64` variants, unless `blas_int = int64` when SLATE was compiled, then
use the `ilp64` variants.

The Makefile detects whether SLATE was compiled with CUDA or not, and
links with CUDA if needed.

You may need to change the CXX compiler in the Makefile.

3. Compile and run examples.

    slate-tutorial>  make -k

    slate-tutorial>  make run

Examples of SLATE's C and Fortran APIs are in the c_api and fortran
directories. They use the same `make.inc` file and can be compiled
similarly.
